#!/bin/bash

if [ -f data/db.json ];
then
  echo "DB alreay in place. Not creating"
else
  echo "Creating DB"
  mkdir data # This will fail if directory already exists, but that's fine
  cp db.sample.json data/db.json
  echo "Created DB"
fi
