FROM node:6.9.4

RUN ["mkdir", "app"]
COPY . app/
WORKDIR app
RUN ["npm", "install"]
RUN ["npm", "run", "build-prod"]

CMD ["npm", "run", "start:service"]