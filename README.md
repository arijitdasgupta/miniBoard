# miniBoard
A little post-comment board application that is easy to deploy and use.

Dependencies:
 - Recommended node version `>=5.0.0`
 - Recommended npm version `>=3.0.0`
 - The `npm` scripts won't work on Windows since there are bash scripts with a shebang

Notes:
 - No password needed. Just enter whatever to login, as of now.
 - To add a new user POST to `:5000/users/` this, `{"email": "<email>", "name": "<name>"}``. Then use the email to login to the application.
 - `curl -H "Content-Type: application/json" -X POST -d '{"email":"<EMAIL>","name":"<NAME>"}' http://localhost:5000/users/`
 - There is a dummy database with data already checked in.
 - If you want to put in custom config, run `npm start` once. That will copy the `config.sample.js` file to `config.js`. You can edit that and run `npm start` again.
 - `npm start` will build the UI.
 - The application runs on `7000` by default. You can change it in `config.js`.
 - The DB runs on `5000`. That can be changed too, but you will need to change in the `npm` scripts also.

```bash
#Install dependencies
npm install
```

```bash
#To run the database, it will copy the sample DB if there aren't any
npm run start-db
```

```bash
#To start the backend
npm start
```

```bash
#To run the front-end dev mode
npm run watch
#or
npm run watch-bs # to start browsersync, currently might not work with the backend
```

```bash
#To run the system indefinitely
npm run build
npm start-forever
#To stop...
npm stop-forever #TODO: fix the db run issue
```

```bash
#To build for production
npm run build-prod
```

TODO:
 - Maybe add front end caching
 - Write a Dockerfile for easy deployment
 - Write actual DB deps and interfaces (no more mock data and interactions)
 - Make the design responsive for mobile, tablets etc
 - Write unit tests
 - Add editing, deleting feature
 - Add user registration, currently it's a direct POST to the database /users/ endpoint.
 - Enable Markdown
 - Offline storage inside the browser.
