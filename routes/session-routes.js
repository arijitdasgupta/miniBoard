(function (global) {
  'use strict'

  var express = require('express')

  var session = require('../lib/session.js')
  var responder = require('../lib/responder.js')

  var router = express.Router()

  // Creates a session with the server
  router.post('/', function (req, res) {
    responder.sendBackResponse(req, res, session.attachACookie)
  })

  // Deletes the existing session
  router.delete('/', function (req, res) {
    responder.sendBackResponse(req, res, session.detachCookie)
  })

  // Gets the details of the current session
  router.get('/', function (req, res) {
    responder.sendBackResponse(req, res, session.getSession)
  })

  module.exports = router
})(global)
