(function (global) {
  'use strict'

  var express = require('express')

  var data = require('../lib/data.js')
  var responder = require('../lib/responder.js')

  var router = express.Router()

  // Get post of posts
  router.get('/posts', function (req, res) {
    responder.sendBackResponse(req, res, data.getPosts)
  })

  // Get a posts
  router.get('/posts/:postId', function (req, res) {
    responder.sendBackResponse(req, res, data.getPost, req.params.postId)
  })

  // Get comment of a post
  router.get('/posts/:postId/comments', function (req, res) {
    responder.sendBackResponse(req, res, data.getCommentsByPost, req.params.postId)
  })

  // Get posts by a user
  router.get('/user-posts/:userId', function (req, res) {
    responder.sendBackResponse(req, res, data.getPostsByUser, req.params.userId)
  })

  // Get users
  router.get('/users', function (req, res) {
    responder.sendBackResponse(req, res, data.getUsers)
  })

  // Get a user
  router.get('/users/:userId', function (req, res) {
    responder.sendBackResponse(req, res, data.getUserById, req.params.userId)
  })

  // Post a post
  router.post('/posts', function (req, res) {
    responder.sendBackResponse(req, res, data.postPost, req.body)
  })

  // Post a comment
  router.post('/comments', function (req, res) {
    responder.sendBackResponse(req, res, data.postComment, req.body)
  })

  // Post a post
  router.put('/posts/:postId', function (req, res) {
    responder.sendBackResponse(req, res, data.editPost, req.body)
  })

  // Post a comment
  router.put('/comments/:commentId', function (req, res) {
    responder.sendBackResponse(req, res, data.editComment, req.body)
  })

  module.exports = router
})(global)
