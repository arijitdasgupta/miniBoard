(function (global) {
  'use strict'

  var express = require('express')

  // Build is the build directory it will serve the status assets from
  // `fallback` will always fallback to index.html if no route is found
  // Useful to directly going to a link in an SPA where the routes are specifically
  // designed to be used in-app only
  module.exports = {
    'build': express.static('build'),
    'fallback': express.static('build/index.html')
  }
})(global)
