var gulp = require('gulp')
var sass = require('gulp-sass')
var concat = require('gulp-concat')
var plumber = require('gulp-plumber')
var browserSync = require('browser-sync')
var sourcemaps = require('gulp-sourcemaps')
var runSequence = require('run-sequence')
var rimraf = require('gulp-rimraf')
var path = require('path')
var ngHtml2Js = require('gulp-ng-html2js')
var historyApiFallback = require('connect-history-api-fallback')
var replace = require('gulp-replace')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')

var config = require('./config.js')

var buildPaths = {
  javascriptVendors: [
    'node_modules/angular/angular.js',
    'node_modules/angular-ui-router/release/angular-ui-router.js',
    'node_modules/ng-orwell/Orwell.js',
    'node_modules/lodash/lodash.js'
  ],
  javascript: [
    'assets/js/app.js',
    'assets/js/**/*.js',
    'assets/components/**/*.js'
  ],
  vendorStylesheets: [
    'assets/styles/typography.css',
    'assets/styles/foundation.css'
  ],
  mainStylesheet: 'assets/styles/app.scss',
  templates: [
    'assets/**/*.html'
  ],
  static: [
    'assets/*.html'
  ],
  build: 'build'
}

var productionBuild = false;

var uglifyPipe = function (stream) {
  return productionBuild?stream.pipe(ngAnnotate()).pipe(uglify()):stream
}

var diPipe = function (stream) {
  return productionBuild?
    stream.pipe(replace(/\%\%ngStrictDi\%\%/g, 'ng-strict-di')):
    stream.pipe(replace(/\%\%ngStrictDi\%\%/g, ''))
}

function getAssetsPath (dirName) {
  return path.join(buildPaths.build, 'assets', dirName)
}

gulp.task('build:clean', function () {
  return gulp.src(buildPaths.build)
    .pipe(rimraf())
})

gulp.task('build:sass', function () {
  return gulp.src(buildPaths.mainStylesheet)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(concat('app.css'))
    .pipe(gulp.dest(getAssetsPath('styles')))
})

gulp.task('build:vendorjs', function () {
  var genBuild = gulp.src(buildPaths.javascriptVendors)
    .pipe(plumber())

  return uglifyPipe(genBuild).pipe(concat('vendor.js'))
    .pipe(gulp.dest(getAssetsPath('js')))
})

gulp.task('build:js', function () {
  var genBuild = gulp.src(buildPaths.javascript)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())

  return uglifyPipe(genBuild).pipe(concat('app.js'))
    .pipe(gulp.dest(getAssetsPath('js')))
})

gulp.task('build:templates', function () {
  return gulp.src(buildPaths.templates)
    .pipe(plumber())
    .pipe(ngHtml2Js({
      moduleName: 'miniBoard.templates',
      declareModule: true
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest(getAssetsPath('js')))
})

gulp.task('build:copy-vendor-stylesheets', function () {
  return gulp.src(buildPaths.vendorStylesheets)
    .pipe(plumber())
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest(getAssetsPath('styles')))
})

gulp.task('build:copy', function () {
  var genStream = gulp.src(buildPaths.static)
    .pipe(replace(/\%\%basePath\%\%/g, config.basePath));
  return diPipe(genStream)
    .pipe(plumber())
    .pipe(gulp.dest(buildPaths.build))
})

gulp.task('build', function (cb) {
  runSequence('build:clean',
    ['build:sass', 'build:js', 'build:vendorjs', 'build:copy-vendor-stylesheets', 'build:copy', 'build:templates'], cb)
})

gulp.task('build-prod', function (cb) {
  productionBuild = true;
  gulp.run('build');
})

gulp.task('watch', function (cb) {
  gulp.watch('assets/**/*', ['build'])

  cb()
})

gulp.task('watch-bs', function (cb) {
  gulp.watch('assets/**/*', ['lock-and-load'])

  cb()
})

gulp.task('lock-and-load', function (cb) {
  runSequence('build', ['reload-browsers'], cb)
})

gulp.task('reload-browsers', browserSync.reload)

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: buildPaths.build,
      middleware: [historyApiFallback()]
    }
  })
})

gulp.task('default', ['build', 'watch'])
