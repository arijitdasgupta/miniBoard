#!/bin/bash

if [ -f config.js ];
then
  echo "Config already in place"
else
  echo "Creating config.js"
  cp config.sample.js config.js
  echo "Created config.js"
fi
