(function (global) {
  'use strict'

  /**
   * [log Logger with a data printed in fron of the string]
   * @return {args} [arugments, just like you would use in console.log]
   */
  var log = function () {
    var args = Array.prototype.slice.call(arguments)
    args.unshift((new Date()).toString())
    console.log.apply({}, args)
  }

  module.exports = {
    log: log
  }
})(global)
