(function (global) {
  'use strict'

  /**
   * [sendBackResponse Takes a req, res, and a function & it's arguments.
   * Calls the function, resolve, rejects handling is pushed to `res.send`]
   * @return {undefined}
   */
  var sendBackResponse = function () {
    var args = Array.prototype.slice.call(arguments)
    var req = args.shift()
    var res = args.shift()
    var jobFunk = args.shift()
    var jobFunkArgs = args

    jobFunkArgs.unshift(res)
    jobFunkArgs.unshift(req)

    jobFunk.apply(this, jobFunkArgs).then(function (resp) {
      res.send(resp)
    }).catch(function (err) {
      res.send(err)
    })
  }

  module.exports = {
    sendBackResponse: sendBackResponse
  }
})(global)
