(function (global) {
  'use strict'

  var _ = require('lodash')
  var Q = require('q')
  var courier = require('./courier.js')
  var config = require('../config.js')

  const loginCookieName = 'login'
  const loginCookiePath = {path: config.basePath + '/api'}

  // Attaches a session to the Express response object if needed
  // Rejects the promise retured when there is a problem.
  // 500 if it can't reach the database to get the usernames
  // 403 is the credentials are wrong
  var attachACookie = function (req, res) {
    var deferred = Q.defer()
    if (isValidCreds(req.body)) {
      var username = req.body.username
      isActualCreds(username).then(function (user) {
        res.cookie(loginCookieName, calculateCookieValue(username), _.assign({maxAge: 3600000}, loginCookiePath))
        res.status(200)
        deferred.resolve({'user': user})
      }).catch(function () {
        res.status(403)
        deferred.reject({'error': 'incorrect-credentials'})
      })
    } else {
      res.status(403)
      deferred.reject({'error': 'wrong-credentials'})
    }
    return deferred.promise
  }

  // Attaches a deletion cookie with the response object
  var detachCookie = function (req, res) {
    var deferred = Q.defer()
    if (isValidCookies(req.cookies)) {
      var username = parseUsernameFromCookie(req.cookies)
      res.cookie(loginCookieName, '', loginCookiePath)
      deferred.resolve({'logged-out': username})
    } else {
      res.status(500)
      deferred.reject({'error': 'invalid-cookies'})
    }
    return deferred.promise
  }

  // Gets the action session from the cookie that's coming in
  // from the expressJS request object
  var getSession = function (req, res) {
    var deferred = Q.defer()
    if (isValidCookies(req.cookies) && isValidLogin(req.cookies)) {
      var cookie = req.cookies[loginCookieName]
      isActualCreds(cookie).then(function (user) {
        deferred.resolve({'user': user})
      }).catch(function () {
        res.status(500)
        deferred.reject({'user': null})
      })
    } else {
      res.status(403)
      deferred.reject({'user': null})
    }
    return deferred.promise
  }

  // To be used for by other lib modules

  /**
   * [isAuthorized Returns a promise that resolves when the request is authorized
   * And reject when it's not]
   * @param  {[express request]}  req [the request object that needs to be checked]
   * @return {promise}     [The promise that will resolve or reject aptly]
   */
  var isAuthorized = function (req) {
    var username = req.cookies[loginCookieName]
    return isActualCreds(username)
  }

  module.exports = {
    attachACookie: attachACookie,
    detachCookie: detachCookie,
    getSession: getSession,
    isAuthorized: isAuthorized
  }

  // UTILS
  /**
   * [getAllUsers Returns a promise that resolves to all the users array]
   * @return {[promise]} [The promise that will resolve to all the users]
   */
  var getAllUsers = function () {
    return courier.mkRequest({
      uri: config.dataUrl + 'users/',
      method: 'GET',
      json: true
    })
  }

  /**
   * [isActualCreds Checks if the credentials in the cookie is actually valid
   * and resolves of rejects the promise as such]
   * @param  {[string]}  username [The username (cookie data) to check]
   * @return {promise}          [The promise that will resolve with the user data if valid
   * or reject if not]
   */
  var isActualCreds = function (username) {
    var def = Q.defer()
    getAllUsers().then(function (resp) {
      var users = _.filter(resp, (item) => item.email === username.toLowerCase())
      if (users.length > 0) {
        def.resolve(users[0])
      } else {
        def.reject()
      }
    }).catch(function () {
      def.reject()
    })

    return def.promise
  }

  /**
   * [isValidCreds Checks if the body with valid credentials]
   * @param  {[object]}  body [The POST body]
   * @return {Boolean}      [Whether is valid credential body]
   */
  var isValidCreds = function (body) {
    return _.has(body, 'username') && _.has(body, 'password')
  }

  /**
   * [calculateCookieValue Calculates the cookie value that's should be returned back]
   * @param  {[string]} username [The username to encode as a cookie]
   * @return {[string]}          [The calculated cookie value]
   */
  var calculateCookieValue = function (username) {
    // As of now it's the username itself
    return username
  }

  /**
   * [isValidCookies Check if the cookie is valid or not]
   * @param  {object}  cookies [The express cookie parser object]
   * @return {Boolean}         [Whether the cookie is valid]
   */
  var isValidCookies = function (cookies) {
    return _.has(cookies, loginCookieName)
  }

  /**
   * [isValidLogin Whether the cookies are of a valid login session]
   * @param  {object}  cookies [The cookie object to check]
   * @return {Boolean}         [Whether is cookie is actually credible cookie]
   */
  var isValidLogin = function (cookies) {
    return _.trim(cookies[loginCookieName]) !== ''
  }

  /**
   * [parseUsernameFromCookie Gets the username from the cookie]
   * @param  {object} cookies [The cookie object to parse username from]
   * @return {string}         [The user-info in this case username]
   */
  var parseUsernameFromCookie = function (cookies) {
    // Since the username is the only cookie value we store as of now
    return cookies[loginCookieName]
  }
})(global)
