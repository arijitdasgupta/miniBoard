(function () {
  'use strict'

  var rp = require('request-promise')
  var Q = require('q')

  /**
   * [getFailurePromise Takes the error and returns a promise]
   * @param  {[object]} err [The error object to be thrown]
   * @return {[promise]}     [The promise that will get rejected with the error information]
   */
  var getFailurePromise = function (err) {
    var def = Q.defer()
    def.reject(err)
    return def.promise
  }

  /**
   * [mkRequest Makes a request described in options,
   * catches errors and resolves, rejects]
   * @param  {[Object]} options [The options for the request]
   * @return {[Promise]}         [The promise of resolve reject]
   */
  var mkRequest = function (options) {
    // The reason there is a try catch block is because something the request library
    // fails with unhandles exception like network outage etc.
    try {
      return rp(options)
    } catch (err) {
      return getFailurePromise(err)
    }
  }

  module.exports = {
    mkRequest: mkRequest
  }
})()
