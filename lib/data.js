(function (global) {
  'use strict'

  var _ = require('lodash')
  var Q = require('q')

  var courier = require('./courier.js')
  var session = require('./session.js')
  var config = require('../config.js')

  var dataUrl = config.dataUrl

  // Here all the action specifications are definied.
  // action specs are essentially what to do with the database
  var actionSpecs = {
    getPosts: function () {
      return {
        uri: dataUrl + 'posts/',
        method: 'GET',
        json: true
      }
    },
    getPostsByUser: function (userId) {
      return {
        uri: dataUrl + 'posts?userId=' + userId,
        method: 'GET',
        json: true
      }
    },
    getCommentsByPost: function (postId) {
      return {
        uri: dataUrl + 'comments?postId=' + postId,
        method: 'GET',
        json: true
      }
    },
    getPost: function (postId) {
      return {
        uri: dataUrl + 'posts/' + postId,
        method: 'GET',
        json: true
      }
    },
    postPost: function (post) {
      return {
        url: dataUrl + 'posts/',
        method: 'POST',
        body: post,
        json: true
      }
    },
    postComment: function (comment) {
      return {
        uri: dataUrl + 'comments/',
        method: 'POST',
        body: comment,
        json: true
      }
    },
    getUsers: function () {
      return {
        uri: dataUrl + 'users/',
        method: 'GET',
        json: true
      }
    },
    getUserById: function (userId) {
      return {
        uri: dataUrl + 'users/' + userId,
        method: 'GET',
        json: true
      }
    },
    editPost: function (post) {
      return {
        uri: dataUrl + 'posts/' + post.id,
        method: 'PUT',
        body: post,
        json: true
      }
    },
    editComment: function (comment) {
      return {
        uri: dataUrl + 'comments/' + comment.id,
        method: 'PUT',
        body: comment,
        json: true
      }
    }
  }

  var exportable = {}
  // Here the action specifications are wrapped inside another function
  // That will determine whether the user is authorized that make that request to the
  // data api. If so then it acts normally, if not rejects the promise that's returned.
  //
  // All the action functions take req, res as first two arguments along with the params that
  // they need to perform that action
  _.forEach(actionSpecs, function (funk, key) {
    exportable[key] = function () {
      var def = Q.defer()
      var args = Array.prototype.slice.call(arguments)
      var req = args.shift()
      var res = args.shift()
      session.isAuthorized(req).then(function () {
        courier.mkRequest(funk.apply(this, args)).then(function (resp) {
          def.resolve(resp)
        }).catch(function (err) {
          res.status(500)
          def.resolve(err)
        })
      }).catch(function () {
        res.status(403)
        def.resolve({status: 'unauthorized'})
      })

      return def.promise
    }
  })

  module.exports = exportable
})(global)
