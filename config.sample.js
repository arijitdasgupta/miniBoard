(function () {
  'use strict'

  module.exports = {
    PORT: 7000, // The port where the application should run
    basePath: '/', // The base path of the application when deployed
    dataUrl: 'http://localhost:5000/' // The database URL
  }
})()
