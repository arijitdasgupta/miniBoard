(function (global) {
  'use strict'

  // Loading node dependencies
  var express = require('express')
  var bodyParser = require('body-parser')
  var cors = require('cors')
  var cookieParser = require('cookie-parser')

  // Loading application modules
  var sessionRoutes = require('./routes/session-routes.js')
  var dataRoutes = require('./routes/data-routes.js')
  var fileRoutes = require('./routes/files-routes.js')
  var logger = require('./lib/logger.js')
  var config = require('./config.js')

  // Loading up all the config etc.
  var PORT = config.PORT
  var basePath = config.basePath

  // Setting the base path for setup the express app routes
  var baseApiPath = basePath + '/api'

  // Initiating applications and added parsers for easy access to those objects
  var app = express()
  app.use(bodyParser.json())
  app.use(cors())
  app.use(cookieParser())

  // Adding routes to the application
  app.use(baseApiPath + '/session', sessionRoutes)
  app.use(baseApiPath + '/data', dataRoutes)
  app.use(basePath, fileRoutes.build)
  app.use(basePath + '*', fileRoutes.fallback)

  // Status endpoint
  app.get(baseApiPath, function (req, res) {
    res.send({
      'status': 'OK'
    })
  })

  // Starting the application
  app.listen(PORT, function () {
    logger.log('Server started on ' + PORT)
  })
})(global)
