(function () {
  'use strict';

  angular.module('miniBoard.components', [
    'miniBoard.components.login',
    'miniBoard.components.infobar',
    'miniBoard.components.post-card',
    'miniBoard.components.post-view',
    'miniBoard.components.post-form',
    'miniBoard.components.comment-form',
    'miniBoard.components.comment-view',
    'miniBoard.components.comments-list'
  ]);
})();
