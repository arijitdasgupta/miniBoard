(function(){
  'use strict';

  angular.module('miniBoard.components.post-form', []).component('postForm', {
    templateUrl: 'components/post-form/post-form.html',
    controllerAs: 'postForm',
    controller: postFormController,
    require: '^HomeController'
  });

  function postFormController($log, $scope, Orwell, PostsService){
    'ngInject';

    var self = this;
    var home = $scope.$parent;

    // Initiate the data for the scope
    var initData = function () {
      self.post = {};
      self.post.title = '';
      self.post.body = '';
    };
    initData();

    // Saves the post that's in the form
    self.savePost = function(){
      self.post.userId = Orwell.getObservable('session').getContent().user.id;

      PostsService.postPost(self.post).then(function () {
        // Toggles the newPost mode
        home.newPost();
        // Reinitiates the home controller so that the new post is shown
        home.init();

        initData();
      }).catch(function (err) {
        // TODO: handle the error
      });
    };
  }
})();
