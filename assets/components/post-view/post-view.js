(function(){
  'use strict';

  angular.module('miniBoard.components.post-view', []).component('postView', {
    templateUrl: 'components/post-view/post-view.html',
    controllerAs: 'postView',
    bindings: {
      post: '='
    }
  });
})();
