(function(){
  'use strict';

  angular.module('miniBoard.components.post-card', []).component('postCard', {
    templateUrl: 'components/post-card/post-card.html',
    controllerAs: 'postCard',
    bindings: {
      post: '='
    }
  });
})();
