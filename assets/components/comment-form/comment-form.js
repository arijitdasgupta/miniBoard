(function(){
  'use strict';

  angular.module('miniBoard.components.comment-form', []).component('commentForm', {
    templateUrl: 'components/comment-form/comment-form.html',
    controllerAs: 'commentForm',
    controller: commentFormController,
    bindings: {
      post: '=',
      saveCallback: '='
    }
  });

  function commentFormController($log, $scope, Orwell, PostsService){
    'ngInject';

    var self = this;
    var home = $scope.$parent;

    var initData = function () {
      self.comment = {};
      self.comment.body = '';
    };
    initData();

    // Saves the comment that's entered the in form
    self.saveComment = function(){
      self.comment.email = Orwell.getObservable('session').getContent().user.email;
      self.comment.postId = self.post.id;

      PostsService.postComment(self.comment).then(function () {
        // After the form is saved, a callback is called to let the user-directive of this component
        // know that a comment has just been posted
        self.saveCallback();

        initData();
      }).catch(function (err) {
        // TODO: handle the error
      });
    };
  }
})();
