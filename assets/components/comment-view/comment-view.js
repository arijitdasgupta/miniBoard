(function(){
  'use strict';

  angular.module('miniBoard.components.comment-view', []).component('commentView', {
    templateUrl: 'components/comment-view/comment-view.html',
    controllerAs: 'commentView',
    controller: commentViewController,
    bindings: {
      comment: '='
    },
  });

  function commentViewController (_, UsersService, $log) {
    'ngInject';

    var self = this;
    var userEmail = self.comment.email;
    self.userObject = null;

    var init = function () {
      // This is to load the user who made that comment.
      // This is less that ideal since it makes so many request to the backend, but
      // As of now it kinda works...

      UsersService.getUsers().then(function (resp) {
        self.userObject = _.filter(resp, function (item) {
          return item.email === userEmail;
        })[0];
      });
    };

    init();
  }
})();
