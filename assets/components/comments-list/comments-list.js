(function(){
  'use strict';

  angular.module('miniBoard.components.comments-list', []).component('commentsList', {
    templateUrl: 'components/comments-list/comments-list.html',
    controllerAs: 'commentsList',
    bindings: {
      comments: '='
    }
  });
})();
