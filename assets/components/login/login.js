(function(){
  'use strict';

  angular.module('miniBoard.components.login', []).component('login', {
    templateUrl: 'components/login/login.html',
    controller: loginController
  });

  function loginController($log, $scope, $state, $rootScope, $location, SessionService){
    'ngInject';

    $scope.username = '';
    $scope.password = '';
    $scope.apiError = null;

    // Create the session by POSTing username:password to the server
    $scope.makeLogin = function () {
      $scope.apiError = false;
      SessionService.createSession($scope.username, $scope.password).then(function(resp){
        $location.path($rootScope.last403Location);
      }).catch(function (){
        $scope.apiError = true;
      });
    };
  }
})();
