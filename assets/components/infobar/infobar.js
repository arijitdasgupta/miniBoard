(function(){
  'use strict';

  angular.module('miniBoard.components.infobar', []).component('infoBar', {
    templateUrl: 'components/infobar/infobar.html',
    controller: topBarController
  });

  function topBarController($log, $scope, $state, $rootScope, SessionService, Orwell){
    'ngInject';

    var sessionObservable = Orwell.getObservable('session');

    // Listens to any change in the sessionObservable, and
    // sets it's own properties accordingly, so that it shows what's needed
    // When apt.
    sessionObservable.addObserver(function(obj){
      $scope.user = obj.user;
    });

    $scope.logout = function(){
      SessionService.killSession().then(function(){
        $state.go('login');
      });
    };
  }
})();
