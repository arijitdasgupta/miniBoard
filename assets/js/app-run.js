(function(){
  'use strict';

  angular.module('miniBoard').run(function(
    $log, $state, $rootScope, $location,
    EndpointService, ApiServer, SessionService, BasePath){
    'ngInject';

    // Setting the endpoint. Might be useful when the api server is hosted on a different machine
    EndpointService.setEndpoint(ApiServer);

    // Putting $state inside $rootScope so that it can be used in `AuthInterceptor`
    $rootScope.$state = $state;

    // Setting last403Location so that the system knows when to get back to after being logged-out
    // And re-logging-in
    $rootScope.last403Location = ($location.path() === '/login')?'/':$location.path();
    // Listening to state change and setting the last403Location so that it can be gone to
    // when needed
    $rootScope.$on('$stateChangeSuccess', function() {
      if($location.path() !== '/login'){
        $rootScope.last403Location = $location.path();
      }
    });

    // Try to get the session if there is any,
    // It yes, go to the last403Location rather than making the user login again
    SessionService.getSession().then(function(resp){
      $location.path($rootScope.last403Location);
    });

    // Just a debug statement
    $log.debug('API Endpoint set to ', EndpointService.getEndpoint());
  });
})();
