(function(){
  'use strict';

  angular.module('miniBoard.services.posts', []).factory('PostsService', PostsService);

  function PostsService($q, $http, EndpointService){
    'ngInject';

    return {
      /**
       * [function Gets a post by postID]
       * @param  {[string]} postId [The postID that needs to be fetched]
       * @return {[promise]}        [The http promise. When resolved gets the response data]
       */
      getPost: function(postId){
        return $http.get(EndpointService.getEndpoint() + 'data/posts/' + postId).then(function(resp){
          return resp.data;
        });
      },
      /**
       * [function Gets posts by userID]
       * @param  {[string]} userId [The userID to fetch the posts by]
       * @return {[promise]}        [The http promise. When resolved gets the response data]
       */
      getPosts: function(userId){
        return $http.get(EndpointService.getEndpoint() + 'data/user-posts/' + userId).then(function(resp){
          return resp.data;
        });
      },
      /**
       * [function Gets comments of a post by postID]
       * @param  {[string]} postId [The postID to fetch comments of]
       * @return {[promise]}        [The http promise. When resolved gets the response data]
       */
      getComments: function(postId){
        return $http.get(EndpointService.getEndpoint() + 'data/posts/' + postId + '/comments').then(function(resp){
          return resp.data;
        });
      },
      /**
       * [function POSTs a Post to the server]
       * @param  {[object]} post [The whole post]
       * @return {[promise]}      [The http promise of 'when done']
       */
      postPost: function(post) {
        return $http.post(EndpointService.getEndpoint() + 'data/posts', post);
      },
      /**
       * [function POSTs a comment]
       * @param  {[object]} comment [The comment object]
       * @return {[promise]}         [The http promise of 'when done']
       */
      postComment: function(comment){
        return $http.post(EndpointService.getEndpoint() + 'data/comments', comment);
      }
    };
  }
})();
