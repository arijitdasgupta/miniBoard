(function(){
  angular.module('miniBoard').factory('AuthInterceptor', function($q, $window, $rootScope, Orwell) {
    return {
      /**
       * Intercepts any response error, checks if it's 403 Unauthorized,
       * If yes, makes the application go to login state.
       *
       * This is attached as an interceptor in app-config.js
       */
      responseError: function(rejection) {
        var defer = $q.defer();
        if(rejection.status === 403) {
          $rootScope.$state.go('login');
          defer.reject();
          Orwell.getObservable('session').setContent({});
          return defer.promise;
        }
        else {
          return rejection;
        }
      }
    };
  });
})();
