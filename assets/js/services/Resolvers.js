(function(){
  'use strict';

  angular.module('miniBoard.services.resolvers', []).service('Resolvers', PostsService);

  function PostsService($q, Orwell, _, EndpointService, PostsService, UsersService, $stateParams){
    'ngInject';

    return {
      /**
       * [function Resolve a singular post]
       * @param  {[string]} postId [The actual postID to resolve]
       * @return {[promise]}        [Resolveds to the post object]
       */
      resolvePost: function(postId){
        return PostsService.getPost(postId);
      },
      /**
       * [function Resolved all the posts for a user]
       * @param  {[string]} userId [The userID to get the posts of]
       * @return {[promise]}        [Resolves when it loads the data]
       */
      resolvePostsForUser: function(userId){
        return PostsService.getPosts(userId);
      },
      /**
       * [function Resolves user details]
       * @param  {[string]} userId [The userID -> user to resolve]
       * @return {[promise]}        [Resolves when it loads the data]
       */
      resolveUserDetails: function(userId){
        return UsersService.getUser(userId);
      },
      /**
       * [function Returns a promise that's only resolved if a user is logged in
       * Or when he/she logs in ]
       * @return {[promise]} [The promise. Resolved if a user is logged in, or gets logged in]
       */
      userLoggedIn: function(){
        var defer = $q.defer();
        var sessionData = Orwell.getObservable('session').getContent();
        if(_.has(sessionData, 'user') && !!sessionData.user) {
          defer.resolve(sessionData);
        } else {
          var observer = function(data) {
            if(_.has(data, 'user') && !!data.user){
              defer.resolve(data);
              Orwell.getObservable('session').removeObserver(observer);
            }
          };
          Orwell.getObservable('session').addObserver(observer);
        }
        return defer.promise;
      }
    };
  }
})();
