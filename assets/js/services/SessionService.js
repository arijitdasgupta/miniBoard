(function(){
  'use strict';

  angular.module('miniBoard.services.session', []).service('SessionService', SessionService);

  function SessionService($q, $http, $rootScope, EndpointService, ApiServer, Orwell){
    'ngInject';

    var sessionObservable = Orwell.getObservable('session');

    return {
      /**
       * [function Returns the current logged in user]
       * @return {[string]} [Current user who is logged in]
       */
      getCurrentUserSync: function(){
        return currentUser;
      },
      /**
       * [function Gets the session data from the endpoint]
       * @return {[promise]} [Resolved when the session data gets returned]
       */
      getSession: function(){
        return $http.get(EndpointService.getEndpoint() + 'session/').then(function(resp){
          sessionObservable.setContent(resp.data);
          return resp;
        });
      },
      /**
       * [function Creates a session with the server]
       * @param  {[string]} username [Login username]
       * @param  {[string]} password [Login password]
       * @return {[promise]}          [Resolved when the session gets created]
       */
      createSession: function(username, password){
        return $http.post(EndpointService.getEndpoint() + 'session/', {
          username: username,
          password: password
        }).then(function(resp){
          sessionObservable.setContent(resp.data);
          return resp;
        });
      },
      /**
       * [function DELETEs the session]
       * @return {[promise]} [Resolves after the promise has been returned]
       */
      killSession: function(){
        sessionObservable.setContent({});
        return $http.delete(EndpointService.getEndpoint() + 'session/');
      }
    };
  }
})();
