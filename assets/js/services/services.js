(function(){
  'use strict';

  angular.module('miniBoard.services', [
    'miniBoard.services.endpoint',
    'miniBoard.services.posts',
    'miniBoard.services.session',
    'miniBoard.services.users',
    'miniBoard.services.resolvers'
  ]);
})();
