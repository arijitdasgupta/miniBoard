(function(){
  'use strict';

  angular.module('miniBoard.services.endpoint', []).service('EndpointService', EndpointService, _);

  function EndpointService(){
    'ngInject';

    var httpEndpoint = '/';

    /**
     * [function Sets the app wide EndpointService singleton endpoint parameter]
     * @param  {[string]} ep [The endpoint]
     * @return {[undefined]}
     */
    var setEndpoint = function(ep){
      httpEndpoint = ep;
    };

    /**
     * [function Gets the app wide EndpointService singleton endpoint parameter]
     * @return {[string]} [The endpoint that needs to be used in the app]
     */
    var getEndpoint = function(){
      return httpEndpoint;
    };

    return {
      setEndpoint: setEndpoint,
      getEndpoint: getEndpoint
    };
  }
})();
