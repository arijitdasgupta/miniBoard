(function(){
  'use strict';

  angular.module('miniBoard.services.users', []).factory('UsersService', UsersService);

  function UsersService($q, $http, EndpointService){
    'ngInject';

    return {
      /**
       * [function Gets the user data from userId]
       * @param  {[string]} userId [The userId to get]
       * @return {[promise]}        [When resolved gets the user data]
       */
      getUser: function(userId){
        return $http.get(EndpointService.getEndpoint() + 'data/users/' + userId).then(function(resp){
          return resp.data;
        });
      },
      /**
       * [function Gets all the users from the server]
       * @return {[promise]} [When resolved gets an array of all the users from the server]
       */
      getUsers: function(){
        return $http.get(EndpointService.getEndpoint() + 'data/users').then(function(resp){
          return resp.data;
        });
      }
    };
  }
})();
