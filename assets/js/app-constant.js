(function(window){
  'use strict';

  angular.module('miniBoard').constant({
    // Lodash, becoming a Angular dependency
    '_': _,
    // The ApiServer endpoint, might be configurable in the future
    // in case of the API server hosted on a different machine. TODO
    'ApiServer': 'api/',
    // The base path of the webapp
    'BasePath': window.document.querySelectorAll('base')[0].getAttribute('href')
  });
})(window);
