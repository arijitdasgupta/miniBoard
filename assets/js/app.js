(function(){
  'use strict';

  var dependencies = [
    'ui.router',
    'ngOrwell',

    'miniBoard.controllers',
    'miniBoard.components',
    'miniBoard.templates',
    'miniBoard.services'
  ];

  angular.module('miniBoard', dependencies);
})();
