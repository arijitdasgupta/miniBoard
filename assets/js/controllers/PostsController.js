(function(){
  'use strict';

  angular.module('miniBoard.controllers.posts', []).controller('PostsController', function(
    $log, $scope, resolvedPosts, resolvedUser, _){
    'ngInject';
    // Reverses all the posts in reverse chronological order
    $scope.posts = _.reverse(_.sortBy(resolvedPosts, function (item) {
      return item.id;
    }));
    // Sets the user property as the resolvedUser that was being resolved as the part of
    // resolve stage in ui-router
    $scope.user = resolvedUser;
  });
})();
