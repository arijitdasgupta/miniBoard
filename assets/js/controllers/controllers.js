(function(){
  'use strict';

  angular.module('miniBoard.controllers', [
    'miniBoard.controllers.home',
    'miniBoard.controllers.login',
    'miniBoard.controllers.post',
    'miniBoard.controllers.posts',
  ]);
})();
