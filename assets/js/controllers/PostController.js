(function(){
  'use strict';

  angular.module('miniBoard.controllers.post', []).controller('PostController',
    function($log, $scope, $stateParams, $state, _, resolvedPost, PostsService){
      'ngInject';

      $scope.post = resolvedPost;
      $scope.init = init;

      // Initiates the controller, and gets all comments related to this post
      function init () {
        PostsService.getComments($stateParams.postId).then(function (resp) {
          $scope.postComments = resp;
        });
      }

      init();
    });
})();
