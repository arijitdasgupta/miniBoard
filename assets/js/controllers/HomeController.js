(function(){
  'use strict';

  angular.module('miniBoard.controllers.home', []).controller('HomeController',
  function($log, $scope, PostsService, _, Orwell, resolvedSession){
    'ngInject';
    $scope.posts = [];
    $scope.loadedPost = null;
    $scope.loadedPostComments = [];
    $scope.newPostMode = false;

    // Loads a post onto the dashboard page
    $scope.loadPost = function(post) {
      $scope.newPostMode = false;
      _.forEach($scope.posts, function(item) {
        delete item.selected;
      });
      post.selected = true;
      var postId = post.id;
      $scope.loadedPost =
        _.filter($scope.posts, function(item){
          return item.id === postId;
        })[0];

      loadComments(postId);
    };

    // Toggles the newpage view by setting the apt scope property
    $scope.newPost = function(){
      $scope.newPostMode = !$scope.newPostMode;
    };

    // Attaches the init function to be used elsewhere
    // TODO: Check if it's being used anywhere else
    $scope.init = init;

    // Load the comments associated with that postId
    var loadComments = function(postId) {
      PostsService.getComments(postId).then(function(resp) {
        $scope.loadedPostComments = resp;
      });
    };

    // Initiate the controller,
    // Load the stuff that's needed
    function init() {
      var userId = resolvedSession.user.id;
      PostsService.getPosts(userId).then(function(data){
        $scope.posts = _.reverse(_.sortBy(data, function(i){
          return i.id;
        }));
        if($scope.posts.length > 0){
          $scope.loadPost($scope.posts[0]);
        }
      });
    }

    init();
  });
})();
