(function(){
  'use strict';

  angular.module('miniBoard').config(function(OrwellProvider, $httpProvider){
    'ngInject';

    // Setting the AuthInterceptor so that when the session is complete
    // The app goes to login
    $httpProvider.interceptors.push('AuthInterceptor');

    // Creating a app-wide observable that will be set during update-deletion
    // of the session
    OrwellProvider.createObservable('session', {});
  });
})();
