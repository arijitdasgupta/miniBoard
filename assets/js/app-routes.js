(function(){
  'use strict';

  angular.module('miniBoard').config(function($stateProvider, $locationProvider, $urlRouterProvider){
    'ngInject';

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise("/");

    // Route to a single post page
    $stateProvider
    .state('post', {
      url: '/post/:postId',
      templateUrl: 'templates/post.html',
      controller: 'PostController',
      resolve: {
        resolvedPost: function(Resolvers, $stateParams){
          return Resolvers.resolvePost($stateParams.postId);
        }
      }
    })
    // Route to a user's own page
    .state('user', {
      url: '/user/:userId',
      templateUrl: 'templates/posts.html',
      controller: 'PostsController',
      resolve: {
        resolvedPosts: function(Resolvers, $stateParams){
          return Resolvers.resolvePostsForUser($stateParams.userId);
        },
        resolvedUser: function(Resolvers, $stateParams){
          return Resolvers.resolveUserDetails($stateParams.userId);
        }
      }
    })
    // Route to the login page
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginController'
    })
    // Route to the dashboard page
    .state('home', {
      url: '/',
      templateUrl: 'templates/home.html',
      controller: 'HomeController',
      resolve: {
        resolvedSession: function(Resolvers){
          return Resolvers.userLoggedIn();
        }
      }
    });
  });
})();
